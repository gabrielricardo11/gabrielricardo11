﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.Telas.Pedido
{
    class PedidoDataBase
    {
        public object DataBase { get; private set; }

        public string Script
        {
            get
            {
                return script;
            }

            set
            {
                script = value;
            }
        }

        public string List1
        {
            get
            {
                return List;
            }

            set
            {
                List = value;
            }
        }

        internal DataBase Db
        {
            get
            {
                return db;
            }

            set
            {
                db = value;
            }
        }

        public int Salvar(PedidoDTO dto)
        {
            string script =
                @"insert into tb_Produto(id_Produto,nm_Produto,vl_Produto) Values(@id_Produto,@nm_Produto,@vl_Produto)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_Produto", dto.Produto));
            parms.Add(new MySqlParameter("nm_Produto", dto.Produto));
            parms.Add(new MySqlParameter("vl_Produto", dto.Produto));

            DataBase db = new DataBase();
            return (script, parms, db);
        }

        private static int (string script, List<MySqlParameter> parms, DataBase db)
        {
            return db.ExecuteInsertScriptWithPk(script, parms);
        }





    }
}
